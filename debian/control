Source: segyio
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jørgen Kvalsvik <jokva@equinor.com>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10),
               cmake,
               dh-python,
               python3 (>= 3.4),
               python3-all-dev,
               python3-numpy,
               python3-pytest,
               python3-pytest-runner,
               python3-setuptools,
               python3-setuptools-scm
Standards-Version: 4.3.0
Vcs-git: https://salsa.debian.org/science-team/segyio.git
Vcs-browser: https://salsa.debian.org/science-team/segyio
Homepage: https://github.com/statoil/segyio
Testsuite: autopkgtest-pkg-python

Package: libsegyio1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: SEG-Y read/write library for seismic processing (runtime)
 segyio is a fast and practical library for reading and writing seismic data in
 SEG-Y format. segyio is random access oriented for modern computers with
 simple read/write primitives on lines.

Package: libsegyio-dev
Architecture: any
Section: libdevel
Depends: libsegyio1 (= ${binary:Version}),
         ${misc:Depends}
Description: SEG-Y read/write library for seismic processing (development)
 segyio is a fast and practical library for reading and writing seismic data in
 SEG-Y format. segyio is random access oriented for modern computers with
 simple read/write primitives on lines.
 .
 This package includes the header files and the static library.

Package: segyio-bin
Architecture: any
Depends: libsegyio1 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: SEG-Y read/write library for seismic processing (shell utilities)
 segyio is a fast and practical library for reading and writing seismic data in
 SEG-Y format. segyio is random access oriented for modern computers with
 simple read/write primitives on lines.
 .
 This package contains useful shell programs.

Package: python3-segyio
Architecture: any
Section: python
Depends: ${shlibs:Depends},
         ${python3:Depends},
         ${misc:Depends}
Description: SEG-Y read/write library for seismic processing (python3 bindings)
 segyio is a fast and practical library for reading and writing seismic data in
 SEG-Y format. segyio is random access oriented for modern computers simple
 read/write primitives on lines.
 .
 This package contain the python3 interface.
